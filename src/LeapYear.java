import java.util.Scanner;

public class LeapYear {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Enter the year you want to check: "); // вводимо рік
        int year = in.nextInt();

        if (year % 4 != 0) { // робимо перший крок, якщо не ділиться, переходимо на п'ятий крок
            System.out.println("The year " + year + " is not the leap year (365 days).");
        } else {
            if ((year % 4 == 0) && (year % 100 != 0)) { // робимо перший крок, якщо ділиться, переходимо на другий крок, перевіряємо чи ділиться на 100, якщо ні - на четвертий крок
                System.out.println("The year " + year + " is the leap year (366 days).");
            } else {
                if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 != 0)) { // робимо другий крок, якщо ділиться, переходимо на третій крок, перевіряємо чи ділиться на 400, якщо ні - на п'ятий крок
                    System.out.println("The year " + year + " is not the leap year (365 days).");
                } else {
                    if ((year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0)) { // інші випадки, що залишились
                        System.out.println("The year " + year + " is the leap year (366 days).");
                    }
                }
            }
        }
    }
}



